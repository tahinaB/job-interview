# Version Française

## Connaissance de base

### Différences entre un process et un thread
- https://www.geeksforgeeks.org/difference-between-process-and-thread/
- https://learn.microsoft.com/en-us/windows/win32/procthread/processes-and-threads

### Principes SOLID

### Architecture hexagonale vs Architecture 3-tiers

### Inversion de dépendances

### MVC vs MVVM

### Type de test (unitaire, integration, E2E, ...)

### Principe du TDD

### Principe du DDD

### Stratégie d'allocation de la mémoire
- 3 [stratégies](https://medium.com/@c.lacroixblum/tout-ce-que-vous-devez-savoir-ou-presque-sur-la-pile-et-le-tas-650334b61021) : 
    - allocation statique
    - allocation dynamique sur la pile (stack)
    - allocation dynamique sur le tas (heap)

## .Net 
### Différences entre .Net Framework, .Net Core, .Net Standard et .Net 5/6/7/8/9

### Languages .Net
- VB
- C#
- F#

### Caractéristiques de C#
- 

### Types de variables en C#
- Type référence
- Type valeur

#### Type valeur
- Elles dérivent toutes de **ValueType** et sont toutes **sealed**
- [Entier](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/integral-numeric-types) : 
    - sbyte (-128 à 127) => **System.SByte**
    - byte (0 à 255) => **System.Byte**
    - short (-32,768 à 32,767) => **System.Int16**
    - ushort (	0 à 65,535) => **System.UInt16**
    - int (	-2,147,483,648 à 2,147,483,647) => **System.Int32**
    - uint (0 à 4,294,967,295) => **System.UInt32**
    - long (-9,223,372,036,854,775,808 à 9,223,372,036,854,775,807) => **System.Int64**
    - ulong (0 à 18,446,744,073,709,551,615) => **System.UInt64**
    - nint => **System.IntPtr**
    - nuint => **System.UIntPtr**
    + pour la représentation hexa, il faut préfixer avec ```0x``` ou ```0X```
- [Virgule flottante](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/floating-point-numeric-types) :
    - float (taille = 4 bytes, précicion = 6 à 9 digits) => **System.Single** => suffixé par ```f``` ou ```F```
    - double (taille = 8 bytes, précicion = 15 à 17 digits) => **System.Double** => suffixé par ```d``` ou ```D```
    - decimal (taille = 16 bytes, précicion = 28 à 29 digits) => **System.Decimal** => suffixé par ```m``` ou ```M```
    + **float** et **double** sont plus utilisé pour de l'optimisation de performance que **decimal** qui est plus pour de la précision
- bool
- char
- Tuple
- Struct
- [enum](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/enum) 

##### Différence entre une varible de type valeur simple et les autres variable de type valeur
- On peut déclarer les variable de type valeur simple comme des constantes contrairement aux autres variables de type valeurs comme les **struct**.

#### Type référence
- object
- string
- delegate
- dynamic
- les types classe
- les interfaces
- les tableaux

#### Différence entre un type valeur et un type référence
- Une variable de type valeur stocke des données et une variable de type référence stocke les références aux données.
- C'est cette différence qui fait que les modifications faitent à l'intérieur d'une fonction n'impacte pas les valeurs d'une variable de type valeur à l'extérieur du dite fonction, vu que c'est juste une copie de sa valeur qui est passée en paramètre. Et le contraire poue une variable de type référence.
- Une variable de type valeur est stocké généralement sur la pile alors que l'autre est sur le tas

#### Différence entre un struct et une classe
- Contrairement à une classe un **struct** ne peut pas hériter d'une classe ou d'un autre struct. Mais il peut implémenter une interface.
- On ne peut pas déclarer un *finalize/destrutor* dans un struct.
- Avant C#11, le constructeur d'un struct devait initialisé tous le champs du struct.

### Avantage de nameof()
- Facilite les refacto de code (renommage des variables)

### Delegate
-

### Portée des classes, methodes et variables
-

### Boxing & Unboxing
- Boxing, c'est le processus pour convertir une variable de type valeur en type référence (object) ou toute autre interface implémentée par cette variable.
- En boxant une variable de type valeur, le CLR l'enveloppe dans un **object** et le stocke dans le Tas managé.
- Unboxing, c'est le processus d'extraction du variable de type valeur depuis **object**.
- Boxing est implicite et Unboxing est par contre explicite.
- Unboxing et boxing sont des processus coûteux en termes de calcul.

### Lazy initialization
- https://learn.microsoft.com/en-us/dotnet/framework/performance/lazy-initialization

### Custom attribute
-

### Safe et Unsafe mode
- Le mot clé **unsafe** est indispensable en C# pour pouvoir utilisé un pointeur dans son code.
- Le mode unsafe désigne une partie du code qui n'est pas gérée par le CLR de .Net.

### Managed et Unmanaged code
- Un code managé est un code qui est exécuté sous la supervision du CLR (Common Language Runtime).
- Un code non managé est un code exécuté en dehors du contexte du CLR, comme les WIN32 DLL
- Dans un code non managé, le développeur est responsable de : 
    - l'allocation mémoire,
    - la vérification des conversion de type,
    - la libération de la mémoire.  

### Comparaison entre ERROR et EXCEPTION

|         |Origin|Types|Handling|Detection time|Impact|Recoverability|
|:-------:|:-----|:----|:-------|:-------------|:-----|:-------------|
|EXCEPTION|- Evénements anticipés du progamme <br>- Facteurs externes attendus(ex des entrées de l'utilisateur dépassant les plages prévues)|- Plusieurs mais dérivent toutes de System.Exception|- Gérer au niveau du code via des Try-Catch|- Pendant la compilation<br>- Pendant l'exécution|- Les EXCEPTIONS ont un impact localisé sur le déroulement du programme. Lorsqu'une exception se produit, l'exécution du programme est interrompue et le système d'exécution recherche un gestionnaire approprié pour traiter l'exception. Si l'exception n'est pas traitée, elle se propage vers le haut jusqu'à ce qu'elle atteigne un bloc de rattrapage approprié ou qu'elle mette fin au programme|- Récupérables et peuvent être anticipées/gérées dans le code.|
|ERROR|- Limitations externes imprévues(manque de ressources,...)<br>- Problèmes de système|- Runtime error <br>- Syntax error <br>- User error|- A gérer en dehors du programme car souvent cela requiert des interventino à un niveau supérieur (ex mise à jour du système,...)|- Identifiable qu'au moment de l'exécution du code problématique.|- Les ERRORS ont un impact plus large, affectant potentiellement l'ensemble du programme ou du système. Elles peuvent entraîner l'arrêt du programme, un comportement anormal du système ou la nécessité de redémarrer l'application, voire le système lui-même.|- Généralement intraitablecar elles se situent en dehors du code, par exemple une panne de disque.|

En C# la majorité des ERRORS connues sont gérées par .Net pour être traitées comme des EXCEPTIONS   